package com.training.order.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Data
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long customerId;

	@NotEmpty(message = "FirstName is Mandatory Field")
	private String firstName;

	@NotEmpty(message="Last Name is a required Field")
	private String lastName;

	@NotNull
	@Min(18)
	private int age;

	@NotEmpty
	private String gender;

	@Column(unique = true)
	@NotNull
	@Email(message = "Please Enter Valid email", regexp = "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9.-]+$")
	private String email;

	@NotNull
	@Pattern(regexp = "[6-9][0-9]{9}", message = "Please enter Valid Indian Phone Number")
	private String phone;

	@NotEmpty(message = "Enter valid Pincode")
	@Size(min = 6, max = 6)
	private String pincode;

	@NotBlank
	private String Address;

}
