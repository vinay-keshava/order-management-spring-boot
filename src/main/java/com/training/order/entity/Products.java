package com.training.order.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Products {

	@Id
	@GeneratedValue
	private long productId;
	
	private String productName;
	
	private float price;
}
