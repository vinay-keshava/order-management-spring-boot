package com.training.order.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.order.entity.Customer;
import com.training.order.service.impl.CustomerServiceImpl;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
@RequestMapping("/customers")
public class CustomerController {

	@Autowired
	CustomerServiceImpl customerServiceImpl;
	
	@PostMapping("/")
	public ResponseEntity<Customer> newCustomer( @RequestBody  @Valid Customer customer){
		return new ResponseEntity<Customer>(customerServiceImpl.saveCustomer(customer),HttpStatus.OK);
		
	}
}
